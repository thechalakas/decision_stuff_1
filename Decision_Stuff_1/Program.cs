﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decision_Stuff_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //some of the code here will already be familiar to you
            //if you are skip ahead

            //the if thing

            //lets get the user to enter a number. and depending on if it is even or not, we will give different types of messages
            Console.WriteLine("Enter a number for the if thing");
            string s = Console.ReadLine();
            int temp_if = Convert.ToInt32(s);

            if(temp_if %2 == 0)  //this means it is odd. this is the starting of if chain
            {
                Console.WriteLine("{0} is even",temp_if); ;
            }
            else if(temp_if % 3 == 0 ) //the if chain continues
            {
                Console.WriteLine("{0} is also divisible by 3", temp_if); ;
            }
            else//the if chain ends here
            {
                Console.WriteLine("{0} is something else", temp_if); ;
            }

            Console.WriteLine("Now doing the while thing");

            //lets do the while, based on the number entered by user
            int i = 5;
            //as long as i is less than the number that user is entered, the loop will keep looping
            //if the user has entered a number less than 5 (becuase i is 5)
            //the loop wont even be entered
            while(i<temp_if)
            {
                Console.WriteLine("{0} and {1}", i,temp_if);
                i++;
            }

            Console.WriteLine("Now doing the do while thing");

            i = 5;
            //this is just like while but...
            //it will execute once irrespective what value i and user entered number is
            do
            {
                Console.WriteLine("{0} and {1}", i, temp_if);
                i++;
            } while (i < temp_if);


            //lets do the for thing
            Console.WriteLine("Now doing the for thing");
            //here there are three parts
            //i=0, asks the i to start at 0 value
            //i++ indicates by how much i should increase after the loop has ended
            //i<temp_if, indicates the condition to check before continuing with the loop
            for(i=0;i<temp_if;i++)
            {
                Console.WriteLine("{0} and {1}", i, temp_if);
            }

            Console.WriteLine("Now doing the foreach thing");

            //foreach works with a collection or list, where it automatically goes through the entire collection

            //let me get a collection
            List<string> list_of_stuff = new List<string>();
            //adding some items to the list
            list_of_stuff.Add("hello there 1");
            list_of_stuff.Add("hello there 2");
            list_of_stuff.Add("hello there 3");
            list_of_stuff.Add("hello there 4");
            list_of_stuff.Add("hello there 5");

            //now I can use the foreach
            //here the current item will be automatically put in t
            //the loop will automatically start at the beginning and and end at the end
            foreach(string t in list_of_stuff)
            {
                Console.WriteLine("{0}", t);
            }

            //now doing the switch thing
            //and usage of break
            //and usage of go to
            Console.WriteLine("Now doing the switch thing and break and goto");
            Console.WriteLine("Enter 1 or 2 or 3");
            string t_s_t = Console.ReadLine();
            switch(t_s_t)
            {
                case "1":
                    {
                        Console.WriteLine("You have entered 1");
                        break;
                    }
                case "2":
                    {
                        Console.WriteLine("You have entered 2");
                        break;
                    }
                case "3":
                    {
                        Console.WriteLine("You have entered 3. and also jumping to default");
                        goto default; //by using the goto, I can make the code jump 
                    }
                default:
                    {
                        Console.WriteLine("You have entered something else");
                        break;
                    }
            }

            //now doing the null coalescing (man, its hard to pronounce that) operator
            //it helps you to assign a default value when something is null while assigning
            Console.WriteLine("Now doing the null coalescing operator");
            string something_empty = null;
            string something_something = something_empty ?? "wow value";

            Console.WriteLine("something_something is {0} because something_empty is null",something_something);

            //lets do the conditional operator
            Console.WriteLine("Now doing the conditional operator");
            bool some_expression;
            Console.WriteLine("Enter 1 for true 2 for false");
            string s_c_o = Console.ReadLine();
            if(s_c_o == "1")
            {
                some_expression = true;
            }
            else
            {
                some_expression = false;
            }

            //here is the conditional operator.
            //if some_expression is true, the first part is assigned to you_entered
            //if some_expression is false, the second part is assigned to you_entered
            string you_entered = some_expression ? "you entered true" : "you entered false";

            Console.WriteLine("you_entered contains the following {0}", you_entered);


            //stop the console from dissapearing
            Console.ReadLine();
        }
    }
}
